# This is a comment line
import commands, subprocess

# Program definitions and defaults
# The hardwired command to collect the file system statistics
DISK_UTIL = ('/bin/df')

# The dictionary to apply the log file management parameters
config_options= {}

# Command to open the configuration file for managing log files
options = open('/etc/sysconfig/bpa-disk-config','r')

for line in options:
	sysconfig_line = line.split('=')
	if len(sysconfig_line) == 1:
		continue
	elif len(sysconfig_line) > 1:
		config_options[sysconfig_line[0]] = sysconfig_line[1].split(',')
	else:
		print 'Untuneable parameter in the configuration file'
options.close()

# converting keys in dictionary
for parse in config_options.keys():
       config_options[parse] = map(str.rstrip,config_options[parse])

# converting inputed thresholds to numeric values
for threshold in config_options['WARNING']:
	config_options['WARNING'] = ''.join(config_options['WARNING'])
for threshold in config_options['CRITICAL']:
	config_options['CRITICAL'] = ''.join(config_options['CRITICAL'])
for threshold in config_options['RETENTION']:
	config_options['RETENTION'] = ''.join(config_options['RETENTION'])
for threshold in config_options['COMPRESSION_WINDOW']:
	config_options['COMPRESSION_WINDOW'] = ''.join(config_options['COMPRESSION_WINDOW'])

FILESYSTEMS = {}	# array containing the file system statistics of all file systems visible on server
FLLIST = {}		# status of df command output array of the df command for all file system visible on server

# Loop to determine the utilization of each file system 

for CHK_FS in config_options['CHECK_FILESYSTEMS']:
	print
	ARG = "%s %s" % (DISK_UTIL,CHK_FS)
	FSLIST = commands.getstatusoutput(ARG)

	MOUNT_DETAILS = ()
	if FSLIST[0] == 0:
		FILESYSTEMS = FSLIST[1].split('\n')
		MOUNT_DETAILS = FILESYSTEMS[1].split()
		percentage = float(MOUNT_DETAILS[2]) / float(MOUNT_DETAILS[1]) * 100

		for FILE_PREFIX in config_options['FILE_FILTERS']:
	
			if percentage >= float(config_options['CRITICAL']):
				print "checking for ", FILE_PREFIX, " in file system ", CHK_FS, " which is part of ", MOUNT_DETAILS[-1], " is above critical threshold at ", percentage, "%"
				ARG = "find" + CHK_FS + " -type f -mtime " + config_options['RETENTION'] + " -iname " + FILE_PREFIX 
				print ARG
	
			elif percentage >= float(config_options['WARNING']):
				print "checking for ", FILE_PREFIX, " in file system ", CHK_FS, " which is part of ", MOUNT_DETAILS[-1], " is above warning threshold", percentage, "%"
				ARG = "find " + CHK_FS + " -type f -mtime " + config_options['RETENTION'] + " -iname " + FILE_PREFIX 
				print ARG
			else:
				print "the file system ", CHK_FS, " which is part of ", MOUNT_DETAILS[-1], " is not above set threshold at ", percentage, "%"

	elif FSLIST[0] > 0:
		print "The file statistics were not available. Exiting with code: ", FSLIST[0]
	else:
		print "Could not successfully retrieve utilization of server's file systems. exiting..."
