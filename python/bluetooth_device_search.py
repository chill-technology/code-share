import commands

devices = commands.getstatusoutput('/usr/bin/hcitool scan')

# this case runs for a vaid execution of hcitool scan
if devices[0] == 0 and devices[1].startswith('Scanning') == True:
	device_name = ()
	device_mac = ()
	name_set = 0
	mac_set = 0
	devices_located = {}

	device_list = devices[1].split('\t')
	i = 0
	while i < len(devices[1].split('\t')):
		node = device_list[i]
		# Parsing out the first line of the output returned by hcitool which should be Scanning
		if node[-1] == '\n' and node.startswith('Scanning') == True:
			pass

		# Assigns the device name from the input stream
		# This case will identify the name when multiple devices appear
		elif node[-1] == '\n' and node.startswith('Scanning') == False:
			device_name = node[:-1]
			name_set = 1

		# Assigns the device name when it occurs at the end of the line, or is last element
		elif node.count(':') == 0 and node.startswith('Scanning') == False:
			device_name = node
			name_set = 1

		# Assigns the device mac address from the input stream
		elif node.count(':') == 5 and len(node) == 17:
			device_mac = node
			mac_set = 1

		else:
			print 'there was an error encountered on iteration', i, 'with element', node

		# building the device matrix for identified devices
		if name_set == 1 and mac_set == 1:
			devices_located[device_name] = device_mac 
			mac_set = 0
			name_set = 0
		i += 1
elif devices[0] != 0:
	sys.exit

#print 'defined the following keys', devices_located.keys()
#print 'defined the following devices', devices_located.values()

# creates a file handle to create a command file to make connections to scanned devices
fh = open("/etc/bluetooth/scanned_devices","w")

# this variable is set to distinquish between the 
i = 0

# this section of creates a file run from cron to make the connections
for bluetooth_dev in  devices_located:
	connection_cmd = ('/usr/bin/rfcomm connect ' + str(i) + ' '+ devices_located[bluetooth_dev] + ' '+ '1 >/dev/null &')
	fh.write(connection_cmd + '\n')
	i += 1
fh.close()
