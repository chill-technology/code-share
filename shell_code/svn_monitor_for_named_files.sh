#! /bin/bash
# Author: Claude Hill
#
# The script in intended to maintain the name service on 
#  the current server. This script will be run from cron
#  to routinely check for modications to the files in the
#  directory /var/lib/named

# Setting the 
declare -a NS_FILE_STATE

if [ $# -eq 0 ] ; then
	printf "\nThis is script is executed from cron to maintain name server.\n" >> /var/log/svn_Log
elif [[ $1 == "named" || $1 == "/var/lib/named" ]] ; then
	SOURCEDIR=/var/lib/named
	REPOSITORY='file:///var/svn/named_repos'
	for NS_FILE in `find $SOURCEDIR -type f`
	do 
		NS_FILE_STATE=`stat $NS_FILE | grep -i Modify `
		read -a FILE_MODIFY_CHARACTERISTIC <<< "$NS_FILE_STATE"
		FILE_LAST_MODIFY_DATE=${FILE_MODIFY_CHARACTERISTIC[1]}
		FILE_LAST_MODIFY_HOUR=${FILE_MODIFY_CHARACTERISTIC[2]%%:*[0-9]}
		if [[ $FILE_LAST_MODIFY_DATE == `date +%F` && $FILE_LAST_MODIFY_HOUR == `date +%H` ]] ; then
			svn import $NS_FILE $REPOSITORY
			printf "The file $NS_FILE has been updated on ${FILE_MODIFY_CHARACTERISTIC[1]} at ${FILE_MODIFY_CHARACTERISTIC[2]}\n" >> /var/log/svn_log
		fi
	done
else
	printf "\nCurrently not configured to maintain the directory tree $1.\n" >> /var/log/svn_log
fi
