#! /bin/bash

# Similar to the url-track script but will accept several URL as an argument
# The output of this script is cleaner

# For loop allow for the submission of several URLs at the command line
for CHECK_URL in ${@}
do 

# This command determines the HTTP status code and is intended to 
#  provide a high level of error handling
HTTP_CODE=`curl -w '%{http_code}' -o /dev/null -s  $CHECK_URL`

# This case structure processes request based on the HTTP code 
case $HTTP_CODE in
	1[0-9][0-9])
		printf "\nThis is an informational URL request\n"
		printf "Received HTTP code $HTTP_CODE for URL $CHECK_URL\n";;

	2[0-9][0-9])	# Successful URL requests are identified and stats collected
		printf "\nGathering statistics on URL $CHECK_URL\n";;

	3[0-9][0-9])	# Redirected URLs reequests are identifed
		printf "\nGathering statistics on the redirected URL $CHECK_URL\n"
		REDIRECT_URL=`curl --head -s $CHECK_URL | grep -i location:`
		printf "The initial HTTP status code returned: $HTTP_CODE\n"
		printf "The URL $CHECK_URL is redirected to ${REDIRECT_URL#Location:}\n"
		CHECK_URL=${REDIRECT_URL#Location:};;

	4[0-9][0-9])	# Client related errors trapped here
		printf "\n Client error detected in URL $CHECK_URL\n"
		continue;;

	5[0-9][0-9])	# Server related errors trapped here
		printf "\nServer error returned from URL $CHECK_URL\n"
		continue;;

	*)
		printf "\nCould not determine status of URL $CHECK_URL\n";;
esac
curl -w '
	HTTP code\t\t: %{http_code}
	Lookup time\t\t: %{time_namelookup} seconds
	Connect time\t\t: %{time_connect} seconds
	Pretransfer time\t: %{time_pretransfer} seconds
	Starttransfer time\t: %{time_starttransfer} seconds
	Size download\t\t: %{size_download} bytes
	Speed download\t\t: %{speed_download} bytes/s
	Total time\t\t: %{time_total} s
	' -o /dev/null -s -o /dev/null -s $CHECK_URL
echo ""
done
